import React from 'react';
import {
    PermissionsAndroid,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Dimensions,
    Image,
    Button,
} from 'react-native';
import RNFS from 'react-native-fs';
import { RNCamera } from 'react-native-camera';
import Icon from 'react-native-vector-icons/AntDesign';
import { RNFFmpeg } from 'react-native-ffmpeg';
import { MultiSlider } from 'react-native-multi-slider';
import Video from 'react-native-video';
import RNFetchBlob from 'react-native-fetch-blob'
import CameraRoll from "@react-native-community/cameraroll"

export default function Camera() {
    const [cameraZoom, setCameraZoom] = React.useState(0);
    const [cameraType, setCametaType] = React.useState(RNCamera.Constants.Type.back);
    const [selectedSong, setSelectedSong] = React.useState(null);

    const [isRecorded, setIsRecorded] = React.useState(false);
    const [recordedData, setRecordedData] = React.useState([]);
    const [time, setTime] = React.useState(0);
    const [intervalId, setIntervalId] = React.useState();
    const [finalVideoThumbPath, setFinalVideoThumbPath] = React.useState("");

    //test data
    const [picture, setPicture] = React.useState('87');

    const [nonCollidingMultiSliderValue, setNonCollidingMultiSliderValue] = React.useState([0, 0]);
    var yourPicture = require('../../botkin.jpg');

    let cameraRef = React.useRef(null);
    let refVideo = React.useRef(null);
    let screenDimensions = {};

    React.useEffect(() => {
        screenDimensions = {
            ScreenHeight: Dimensions.get('window').height,
            ScreenWidth: Dimensions.get('window').width,
        }
    }, []);

    const requestStoragePermission = async () => {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE, {}
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("You can use the storage");
            } else {
                console.log("Storage permission denied");
            }
        } catch (err) {
            console.log("error permission")
            console.warn(err);
        }
       
    };
    
    const checkFilePath = async filepath => {
        const resExist = await RNFS.exists(filepath);
        console.log("resExist", resExist)
    }

    const getVideo = () => {
        console.log("CameraRoll", CameraRoll)
        CameraRoll.getPhotos({
            assetType: "Videos"
        })
        .then(response => {
            console.log("videos", response)

        })
    }


    React.useEffect(() => {
        if (recordedData.length > 0) {
            // recordedData.forEach(videoData => {
            const videoData = recordedData[0];
            const filepath = videoData.video.uri;
            console.log("filepath ", filepath);
            RNFetchBlob.fs.stat(filepath)
                .then((stats) => {
                    console.log("stats ", stats)
                    console.log("CachesDirectoryPath ", RNFS.CachesDirectoryPath);
                    PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE).then(response => {
                        console.log("permission", response)
                        const outVideoPath = getTestVideoPath()
                        //const outVideoPath = 'file://' + RNFS.CachesDirectoryPath + '/'+stats.filename;
                        checkFilePath(outVideoPath)
                        const path = filepath.slice(0, filepath.length - 4);
                        const thumbGenerationCommand = `-i ${outVideoPath}
                -vf fps=${10 / videoData.time},
                scale=90:-1 -preset veryfast
                ${path}_thumb_%01d.jpg`;
                        console.log("formorate preview")

                        RNFFmpeg.execute(thumbGenerationCommand).then(
                            _ => {
                                console.log("start execute")
                                setFinalVideoThumbPath(`${path}_thumb_`)
                                console.log("end execute")
                            },
                            _err => {
                                console.log("Error generate thumbnails", _err);
                            }
                        )
                    })
                })
                .catch((err) => { })

            // });
        };
    }, [recordedData]);

    const takePicture = async () => {
        if (cameraRef) {
            const options = { quality: 0.5, base64: true };
            const data = await cameraRef.current.takePictureAsync(options);
            setPicture(data.uri);
        }
    };

    const onRecordingEnd = () => {
        console.log("onRecordingEnd");
    };

    const startTimer = () => {
        const intervalId = setInterval(changeTimer, 1000);
        setIntervalId(intervalId);
    };

    const stopTimer = () => {
        clearInterval(intervalId);
        setTime(0);
    };

    const changeTimer = () => {
        setTime(c => c + 1);
    };

    const startRecord = () => {
        if (!isRecorded) {
            console.log("start record")
            startTimer();
            setIsRecorded(true);
            cameraRef.current
                .recordAsync({
                    height: screenDimensions.ScreenHeight,
                    width: screenDimensions.ScreenWidth,
                    quality: RNCamera.Constants.VideoQuality["1080p"],
                    targetBitrate: 1000 * 1000 * 40
                })
                .then(data => {
                    console.log("stop recording to method")
                    let newRecordedData = [...recordedData];
                    const videoData = {
                        video: data,
                        time: time
                    }
                    newRecordedData.push(videoData);
                    setRecordedData(newRecordedData);

                    stopTimer();
                })
                .catch(error => console.log(error));
        } else {
            console.log("stop recording to button")
            cameraRef.current.stopRecording();
            setIsRecorded(false);
        }
    };

    const handleSliderChange = () => {
        console.log("handleSliderChange");
    };

    const renderThumbainls = () => {
        const thumbs = [];
        for (let idx = 1; idx <= 9; idx++) {
            thumbs.push(
                <View style={styles.thumbs} key={`video-thumb-${idx}`}>
                    <Text style={{}}>Image</Text>
                    <Text style={{ fontSize: 8 }}>{`${finalVideoThumbPath}${idx}.jpg`}</Text>
                    {/* <Image
                        source={{ uri: `${finalVideoThumbPath}${idx}.jpg` }}
                        // source={yourPicture}
                        style={styles.image}
                    /> */}
                </View>
            );
        }
        return thumbs;
    };

    const getTestVideoPath = () => {
    return RNFS.ExternalStorageDirectoryPath +'/Movies/inshot/InShot_20210525_230926693.mp4';
    }

    return (
        <View style={styles.container}>
            <Text>This is Camera is greatestr</Text>
            <RNCamera
                ref={cameraRef}
                style={styles.camera}
                useNativeZoom={true}
                zoom={cameraZoom}
                type={cameraType}
                path={`RNFS.LibraryDirectoryPath/quickStories`}
                captureAudio={!selectedSong}
                onRecordingEnd={onRecordingEnd}
                // onRecordingStart={onRecordingStart}
                flashMode={RNCamera.Constants.FlashMode.on}
                hideShutterView={true}
                onCameraReady={() => console.log("camera ready")}
                androidCameraPermissionOptions={{
                    title: 'Permission to use camera',
                    message: 'We need your permission to use your camera',
                    buttonPositive: 'Ok',
                    buttonNegative: 'Cancel',
                }}
                androidRecordAudioPermissionOptions={{
                    title: 'Permission to use audio recording',
                    message: 'We need your permission to use your audio',
                    buttonPositive: 'Ok',
                    buttonNegative: 'Cancel',
                }}
            >
                <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => startRecord()}>
                        {!isRecorded && (
                            <Icon name="closecircle" size={50} color="white" />
                        )}
                        {isRecorded && (
                            <Icon name="closecircle" size={50} color="red" />
                        )}
                    </TouchableOpacity>
                    <TouchableOpacity onPress={takePicture}>
                        <Icon name="picture" size={50} color="red" />
                    </TouchableOpacity>
                </View>
                <Button title="Permission" onPress={requestStoragePermission} />
                <Button title="Videos" onPress={getVideo} />
            </RNCamera>
            <Text>{time}</Text>
            {/* <MultiSlider
                value={[
                    nonCollidingMultiSliderValue[0],
                    nonCollidingMultiSliderValue[1]
                ]}
                style={{opacity: 0.2}}
                sliderLength={screenDimensions.ScreenWidth - 40}
                onValuesChange={handleSliderChange}
                // trackStyle={}
                height={80}
                min={0}
                max={15}
                step={1}
                allowOverlap={false}
                snapped
                enableLable={true}
                minMarkerOverlapDistance={180}
                customMarker={e => (
                    <View currentValue={e.currentValue}/>
                )}
            /> */}
            {!finalVideoThumbPath && (
                <Text>Generate thumbs...</Text>
            )}
            {!!finalVideoThumbPath && (
                <View>{renderThumbainls()}</View>
            )}
            <Image
                source={{
                    uri: picture
                }}
                style={styles.image}
            />
            <Video 
                //source={{uri: getTestVideoPath()}}
                source={{ uri: recordedData.length > 0 ? recordedData[0].video.uri : '12' }}
                ref={refVideo}
                hideShutterView={true}
                resizeMode={"stretch"}
                style={styles.video} />
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        // ...StyleSheet.absoluteFillObject,
        flex: 1,
        justifyContent: "space-between",
        backgroundColor: "#fff",
        margin: 10,
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20,
    },
    camera: {
        flex: 1,
        backgroundColor: "red"
    },
    thumbs: {
        flex: 0,
        padding: 20,
        flexDirection: "row",
        //backgroundColor: "#ccc"
    },
    image: {
        height: 100,
        width: 100
    },
    video: {
        height: 250,
        width: 150
    }
});